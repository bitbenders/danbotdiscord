import {API_URL} from './settings';
import axios from 'axios';
import * as fs from 'fs';

let currentSongPlaying = '';
let songQueue = [];
let channelGenre = -1;
let songGenre = -1;


//i contains search
const getSongList = async (message) => {
        let response = await axios.get(API_URL + '/api/songs/');
            //duplicate song handling
            let comboList = [];
            for (let i of response.data && comboList.length <= 25) {
                comboList.push(i.name)
            }
        const exampleEmbed = new Discord.MessageEmbed()
            .setColor('#424242')
            .addFields(
                { name: 'title', value: 'Song List' },
                { name: '1:', value: comboList[0], inline: true },
                { name: '2:', value: comboList[1], inline: true },
                { name: '3:', value: comboList[2], inline: true },
                { name: '4:', value: comboList[3], inline: true },
                { name: '5:', value: comboList[4], inline: true },
                { name: '6:', value: comboList[5], inline: true },
                { name: '7:', value: comboList[6], inline: true },
                { name: '8:', value: comboList[7], inline: true },
                { name: '9:', value: comboList[8], inline: true },
                { name: '10:', value: comboList[9], inline: true },
                { name: '11:', value: comboList[10], inline: true },
                { name: '12:', value: comboList[11], inline: true },
                { name: '13:', value: comboList[12], inline: true },
                { name: '14:', value: comboList[13], inline: true },
                { name: '15:', value: comboList[14], inline: true },
                { name: '16:', value: comboList[15], inline: true },
                { name: '17:', value: comboList[16], inline: true },
                { name: '18:', value: comboList[17], inline: true },
                { name: '19:', value: comboList[18], inline: true },
                { name: '20:', value: comboList[19], inline: true },
                { name: '21:', value: comboList[20], inline: true },
                { name: '22:', value: comboList[21], inline: true },
                { name: '23:', value: comboList[22], inline: true },
                { name: '24:', value: comboList[23], inline: true },
                { name: '25:', value: comboList[24], inline: true },

            );
           await message.reply(exampleEmbed);
           return;

        //need the post for voting
};

const getRank = (message) => {
    axios.get(API_URL + '/api/songs/rank/' + currentSongPlaying + '/')
        .then(response => {
            message.reply('The current upvotes of ' + currentSongPlaying + 'is: ' + response)
        })
        .catch(error => {
            console.log( error );
        });
};
const songPlaying = (message) => {
    if (currentSongPlaying !== '') {
        message.reply('The current song playing is ' + currentSongPlaying);
    }
    else (message.reply('There is not a song currently playing'));
};

const skip = (message) => {
    songQueue.shift();
    play(message, songQueue[0]);
};

const help = (message) => {
    message.reply('Here is the list of commands: \n' +
        '    \'!getRank\'\n' +
        '    \'!play\'\n' +
        '    \'!upvote\'\n' +
        '    \'!songPlaying\'\n' +
        '    \'!skip\'\n' +
        '    \'!help\'\n' +
        '    \'!songRanks\'\n' +
        '    \'!suggest\'\n' +
        '    \'!suggestRemove\'');
    return;
};

const songRanks = async (message) => {
    await getChannelGenre(message);
    console.log('got genre it is: ' + channelGenre);
    axios.get(API_URL + '/api/songs/genres/' + channelGenre + '/')
        .then(response => {
            console.log( 'success response is: ' + response.data[0].name );
            message.reply('Here are the top 10:\n',
            response.data[0].name
            );
        })
        .catch(error => {
            console.log( error );
        });
};

const suggest = async (message) => {
    axios.post(API_URL + '/api/songSuggestions/suggest/',  {name: message.content.substring(8), discord_id: message.author.id})
    .then(response => {
            console.log('success', response)
        })
    .catch(error => {
        console.log( error );
        message.reply(error.response.data || 'an error occurred suggesting.')
    });
};

const suggestremove = async (message) => {
    axios.post(API_URL + '/api/songs/remove-vote/',  {name: message.content.substring(8), discord_id: message.author.id})
        .then(response => {
            console.log('success', response)
        })
        .catch(error => {
            console.log( error );
            message.reply(error.response.data || 'an error occurred voting to remove.')
        });
};


const getChannelGenre= async (message) =>{
    let voiceChannelName = message.member.voice.channel.name;
    console.log(voiceChannelName);
    try {
        if (voiceChannelName.includes('ChillStep')) {
            channelGenre = 0;
            return
        } else if (voiceChannelName.includes('metal')) {
            channelGenre = 1;
            return
        } else if (voiceChannelName.includes('rock')) {
            channelGenre = 2;
            return
        }
        else {
            console.log('cant find genre')
        }
    }

    catch (e) {
        console.log(e);
    }
    console.log(voiceID,'< voice id chanel');

};



const getSong = async (message) =>{
    try {
        let response = await axios.get(API_URL + '/api/songs/?name=' + encodeURIComponent(message.content.substring(6)));
        getChannelGenre(message);
        if (response.data.length > 1) {
            //duplicate song handling
            let comboList = [];
            for (let i of response.data) {
                comboList.push(i.name)
            }
            message.reply('These duplicate songs were returned, delete one from database or rename it' + comboList.join(', '));
            return;
        }
        else if (response.data.length <= 0) {
            message.reply('No Songs with that name were found');
            return;
        }
        else if (songGenre !== channelGenre){
            message.reply('That song is not the same genre as the channel.');
            return;
        }


        songQueue.push(response.data[0]);
        await message.reply(response.data[0].name + ' has been added to the queue.');
        return response.data[0];
    }
    catch (e) {
        message.reply('There was an error retrieving the song.');
    }
};

const play = async  (message, song) =>{
    let hasPlayed = false;
    if (!message.member.voice.channel)
    {
        message.reply('You need to join a voice channel first!');
        return;
    }

    else{
        const connection = await message.member.voice.channel.join();
        let streamDispatch = connection.play(API_URL + song.song_file);
        currentSongPlaying = song.name;
        songGenre = song.genre;
        streamDispatch.on('speaking', (speaking) => {
            if (speaking===1){
                hasPlayed = true;
            }
           else if (speaking === 0 && hasPlayed) {
                songQueue.shift();
                if(songQueue.length>0){
                    play(message, songQueue[0]);
                }
                else {
                    currentSongPlaying = '';
                }
            }
        })

    }
};

const onSongMessage = async (message) => {
    // Only try to join the sender's voice channel if they are in one themselves
    if (!message.guild) return;

   let song = await getSong(message);
        if (currentSongPlaying === ''){
            play(message, song)
        }


};

const upvote = (message) => {
    axios.post(API_URL + '/api/songs/up-vote/',  {name: message.content.substring(8), discord_id: message.author.id})
        .then(response => {
            console.log('success', response)
        })
        .catch(error => {
            console.log( error );
            message.reply(error.response.data || 'an error occurred upvoting.')
        });
};

const COMMAND_LIST = {
    '!getSongList' :  getSongList,
    '!getRank' : getRank,
    '!play' : onSongMessage,
    '!upvote' : upvote,
    '!songPlaying' : songPlaying,
    '!skip' : skip,
    '!help' : help,
    '!songRanks' : songRanks,
    '!suggest' : suggest,
    '!suggestRemove' : suggestremove,
};

 export default COMMAND_LIST;
