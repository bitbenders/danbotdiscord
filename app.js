import 'babel-polyfill';
import Express from 'express';
import Discord from 'discord.js';

import {BOT_TOKEN, API_URL} from './settings';
import COMMAND_LIST from "./command-list";

import axios from 'axios';

const app = Express();
const client = new Discord.Client({
    commandPrefix: '!',
    unknownCommandResponse: false,
});

client.on('ready', () => {
    console.log( 'bot is connected' );
});

client.on('message', message => {
    console.log('message: ' + message.content);
    console.log(message.content in COMMAND_LIST);
    if(message.content.split(' ')[0] in COMMAND_LIST) {
        console.log(message.content.split(' ')[0]);
        COMMAND_LIST[message.content.split(' ')[0]](message);
    }
});

client.on('guildMemberAdd', (guildMember) => {
    console.log(guildMember.id);

    axios.post(API_URL + '/api/discord-users/', {
        discord_id: guildMember.id
    }).then(response => {
        console.log('success!');
        console.log(response);
    }).catch(err => {
        console.log(err);
    });
});

client.login(BOT_TOKEN);

app.listen(3001, () => console.log('DanBot listening on port 3001!'));
